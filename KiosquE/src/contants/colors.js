export default class Colors {
    static PRIMARY_COLOR = '#004e67';

    static SECONDARY_COLOR = 'black';

    static THIRDARY_COLOR = 'black';

    static BACKGROUND_COLOR = 'white';

    static ERROR_COLOR = 'red'
}